/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Issues.css';
import IssuesSubNavigation from "../../components/IssuesSubNavigation/IssuesSubNavigation";
import {connect} from "react-redux";
import IssueTable from "../../components/IssueTable/IssueTable";
import updateIssues from "../../actions/updateIssues";
import getAvailableLabels from "../../actions/getAvailableLabels";
import getAvailableAuthors from "../../actions/getAvailableAuthors";
import getAvailableMilestones from "../../actions/getAvailableMilestones";
import getAvailableAssignees from "../../actions/getAvailableAssignees";
import Pagination from "../../components/Pagination/Pagination";

const mapDispatchToProps = dispatch => {
  return {
    updateIssues() {
      dispatch(updateIssues());
    },
    getAvailableAuthors() {
      dispatch(getAvailableAuthors());
    },
    getAvailableLabels() {
      dispatch(getAvailableLabels());
    },
    getAvailableMilestones() {
      dispatch(getAvailableMilestones());
    },
    getAvailableAssignees() {
      dispatch(getAvailableAssignees());
    }
  }
};

class Issues extends React.Component {
  static propTypes = {
    updateIssues: PropTypes.func.isRequired,
    getAvailableAuthors: PropTypes.func.isRequired,
    getAvailableLabels: PropTypes.func.isRequired,
    getAvailableMilestones: PropTypes.func.isRequired,
    getAvailableAssignees: PropTypes.func.isRequired
  };

  componentDidMount(){
    this.props.updateIssues();
    this.props.getAvailableAuthors();
    this.props.getAvailableLabels();
    this.props.getAvailableMilestones();
    this.props.getAvailableAssignees();
  }

  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <IssuesSubNavigation/>
          <IssueTable />
          <Pagination />
        </div>
      </div>
    );
  }
}

export default withStyles(s)(connect(() => ({}), mapDispatchToProps)(Issues));
