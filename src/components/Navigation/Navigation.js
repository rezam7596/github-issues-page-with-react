/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Navigation.css';
import Link from '../Link';

class Navigation extends React.Component {
  render() {
    return (
      <div className={s.root} role="navigation">
        <div className={s.leftContainer}>
          <Link className={s.link} to="/features">
            Features
          </Link>
          <Link className={s.link} to="/business">
            Business
          </Link>
          <Link className={s.link} to="/explore">
            Explore
          </Link>
          <Link className={s.link} to="/marketplace">
            Marketplace
          </Link>
          <Link className={s.link} to="/pricing">
            Pricing
          </Link>
        </div>
        <div className={s.rightContainer}>
          <form className={s.form} onSubmit={ event => alert(event.target.value) }>
            <input type="text" placeholder="Search or jump to..." className={s.search}/>
          </form>
          <Link className={cx(s.link, s.signin)} to="/signin">
            Sign in
          </Link>
          <span className={s.spacer}>or</span>
          <Link className={cx(s.link, s.signup)} to="/signup">
            Sign up
          </Link>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Navigation);
