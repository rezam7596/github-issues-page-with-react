/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Pagination.css';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import setCurrentPage from "../../actions/setCurrentPage";

const mapStateToProps = state => {
  return {
    currentPage: state.currentPage,
    numberOfPages: Math.ceil(state.numberOfIssues / state.perPage)
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setCurrentPage(page) {
      dispatch(setCurrentPage(page));
    }
  }
};

class Pagination extends React.Component {
  static propTypes = {
    currentPage: PropTypes.number.isRequired,
    numberOfPages: PropTypes.number.isRequired,
    setCurrentPage: PropTypes.func.isRequired
  };

  render() {
    let availablePages = Array.apply(null, {length: this.props.numberOfPages}).map((x, i) => i+1);
    return (
      <div className={s.container}>
        <div className={s.pagination}>
          {(this.props.currentPage === 1) ?
            <div className={s.disabled}>Previous</div> :
            <div onClick={() => this.props.setCurrentPage(Math.max(1, this.props.currentPage-1))}>
              Previous
            </div>}
          {availablePages.map(page =>
            (this.props.currentPage === page) ?
              <div className={s.selected}>{page}</div> :
              <div onClick={() => this.props.setCurrentPage(page)}>{page}</div>
          )}
          {(this.props.currentPage === this.props.numberOfPages) ?
            <div className={s.disabled}>Next</div> :
            <div onClick={() => this.props.setCurrentPage(Math.min(this.props.numberOfPages, this.props.currentPage+1))}>
              Next
            </div>}
        </div>
      </div>
    );
  }
}

export default withStyles(s)(connect(mapStateToProps, mapDispatchToProps)(Pagination));
