/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Footer.css';
import Link from '../Link';
import logo from './github-logo.svg'

class Footer extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <div>
            <span className={s.text}>© 2018 GitHub, Inc.</span>
            <Link className={s.link} to="/#">
              Terms
            </Link>
            <Link className={s.link} to="/#">
              Privacy
            </Link>
            <Link className={s.link} to="/#">
              Security
            </Link>
            <Link className={s.link} to="/#">
              Status
            </Link>
            <Link className={s.link} to="/#">
              Help
            </Link>
          </div>
          <img src={logo} width="24" height="24" alt="GitHub"/>
          <div>
            <Link className={s.link} to="/#">
              Contact GitHub
            </Link>
            <Link className={s.link} to="/#">
              API
            </Link>
            <Link className={s.link} to="/#">
              Training
            </Link>
            <Link className={s.link} to="/#">
              Contact GitHub
            </Link>
            <Link className={s.link} to="/#">
              Shop
            </Link>
            <Link className={s.link} to="/#">
              Blog
            </Link>
            <Link className={s.link} to="/#">
              About
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Footer);
