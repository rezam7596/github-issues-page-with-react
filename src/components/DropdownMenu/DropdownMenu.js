/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DropdownMenu.css';
import {connect} from "react-redux";

class DropdownMenu extends React.Component {
  static propTypes = {
    isSort: PropTypes.bool,
    headerContent: PropTypes.string.isRequired,
    bodyItems: PropTypes.array.isRequired,
    inputPlaceholder: PropTypes.string,
    itemClicked: PropTypes.func.isRequired,
    selectedItems: PropTypes.array.isRequired
  };
  static defaultProps = {
    isSort: false,
    inputPlaceholder: ''
  };

  constructor(props) {
    super(props);
    this.state = { bodyDisplay: s.hidden };

    this.changeDisplay = this.changeDisplay.bind(this);
  }

  changeDisplay() {
    this.setState({bodyDisplay: (this.state.bodyDisplay !== s.hidden) ? s.hidden : s.shown});
  }

  render() {
    return (
      <div className={s.root}>
        <div className={s.header} onClick={this.changeDisplay}>
          {this.props.headerContent} <i className="fa fa-caret-down"/>
        </div>
        <div className={s.dropdownBackground + ' ' + this.state.bodyDisplay} onClick={this.changeDisplay}/>
        <div className={s.body + ' ' + this.state.bodyDisplay}>
          <div>
            {this.props.isSort ? (
              'Sort by'
            ) : (
              'Filter by ' + this.props.headerContent.toLowerCase()
            )}
          </div>
          <div>
            <input placeholder={this.props.inputPlaceholder}/>
          </div>
          <div>
            <ul>
              {this.props.bodyItems.map(item => (
                <li onClick={() => this.props.itemClicked(item.textForAction)} key={item.textForAction}>
                  {(this.props.selectedItems.includes(item.textForAction)) ? <i className="fas fa-check"/> : ''}
                  {item.textToShow}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(DropdownMenu);
