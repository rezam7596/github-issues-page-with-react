/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './IssuesSubNavigation.css';

class IssuesSubNavigation extends React.Component {
  render() {
    return (
      <div className={s["sub-nav"]}>
        <form className={s.filter}>
          <input type="text"/>
        </form>
        <div className={s["label-milestone"]}>
          <div>Labels</div>
          <div>Milestones</div>
        </div>
        <div className={s["new-issue"]}>
          New issue
        </div>
      </div>
    );
  }
}

export default withStyles(s)(IssuesSubNavigation);
