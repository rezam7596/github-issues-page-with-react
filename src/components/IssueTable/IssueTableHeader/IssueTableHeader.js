/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './IssueTableHeader.css';
import {connect} from "react-redux";
import DropdownMenu from "../../DropdownMenu/DropdownMenu";
import setAuthor from "../../../actions/setAuthor";
import addLabel from "../../../actions/addLabel";
import setProject from "../../../actions/setProject";
import setMilestone from "../../../actions/setMilestone";
import setAssignee from "../../../actions/setAssignee";
import setSortAndDirection from "../../../actions/setSortAndDirection";

const mapStateToProps = (state) => {
  return {
    availableAuthors: state.availableAuthors.map(item => {
      item.textToShow = item.login;
      item.textForAction = item.login;
      return item;
    }),
    availableLabels: state.availableLabels.map(item => {
      item.textToShow = item.name;
      item.textForAction = item.name;
      return item;
    }),
    availableProjects: state.availableProjects.map(item => {
      item.textToShow = item.name;
      item.textForAction = item.name;
      return item;
    }),
    availableMilestones: state.availableMilestones.map(item => {
      item.textToShow = item.title;
      item.textForAction = item.number;
      return item;
    }),
    availableAssignees: state.availableAssignees.map(item => {
      item.textToShow = item.login;
      item.textForAction = item.login;
      return item
    }),
    availableSorts: [ {textToShow:'Newest', textForAction:'Newest'},
      {textToShow:'Oldest', textForAction:'Oldest'},
      {textToShow:'Most commented', textForAction:'Most commented'},
      {textToShow:'Least commented', textForAction:'Least commented'},
      {textToShow:'Recently updated', textForAction:'Recently updated'},
      {textToShow:'Least recently updated', textForAction:'Least recently updated'}
    ],
    selectedAuthor: [state.author],
    selectedLabels: state.labels,
    selectedProject: [state.project],
    selectedMilestone: [state.milestone],
    selectedAssignee: [state.assignee],
    selectedSortAndDirection: [(state.sort === 'created' && state.direction === 'desc') ? 'Newest' : 'Oldest']
  }
};
const mapDispatchToProps = (dispatch) => {
  return {
    setAuthor(value) {
      dispatch(setAuthor(value));
    },
    addLabel(value) {
      dispatch(addLabel(value));
    },
    setProject(value) {
      dispatch(setProject(value));
    },
    setMilestone(value) {
      dispatch(setMilestone(value));
    },
    setAssignee(value) {
      dispatch(setAssignee(value));
    },
    setSortAndDirection(value) {
      dispatch(setSortAndDirection(value));
    }
  }
};

class IssueTableHeader extends React.Component {
  static propTypes = {
    availableAuthors: PropTypes.array.isRequired,
    availableLabels: PropTypes.array.isRequired,
    availableProjects: PropTypes.array.isRequired,
    availableMilestones: PropTypes.array.isRequired,
    availableAssignees: PropTypes.array.isRequired,
    availableSorts: PropTypes.array.isRequired,
    selectedAuthor: PropTypes.array.isRequired,
    selectedLabels: PropTypes.array.isRequired,
    selectedProject: PropTypes.array.isRequired,
    selectedMilestone: PropTypes.array.isRequired,
    selectedAssignee: PropTypes.array.isRequired,
    selectedSortAndDirection: PropTypes.array.isRequired,
    setAuthor: PropTypes.func.isRequired,
    addLabel: PropTypes.func.isRequired,
    setProject: PropTypes.func.isRequired,
    setMilestone: PropTypes.func.isRequired,
    setAssignee: PropTypes.func.isRequired,
    setSortAndDirection: PropTypes.func.isRequired,
  };

  render() {
    return (
      <div className={s["issue-header"]}>
        262 Open
        <div>
          <DropdownMenu isSort={true} headerContent={'Sort'} bodyItems={this.props.availableSorts}
                        itemClicked={this.props.setSortAndDirection}
                        selectedItems={this.props.selectedSortAndDirection}/>
        </div>
        <div>
          <DropdownMenu headerContent={'Assignee'} bodyItems={this.props.availableAssignees}
                        inputPlaceholder={'filter users'} itemClicked={this.props.setAssignee}
                        selectedItems={this.props.selectedAssignee}/>
        </div>
        <div>
          <DropdownMenu headerContent={'Milestones'} bodyItems={this.props.availableMilestones}
                        inputPlaceholder={'filter milestones'} itemClicked={this.props.setMilestone}
                        selectedItems={this.props.selectedMilestone}/>
        </div>
        <div>
          <DropdownMenu headerContent={'Projects'} bodyItems={this.props.availableProjects}
                        inputPlaceholder={'filter projects'} itemClicked={this.props.setProject}
                        selectedItems={this.props.selectedProject}/>
        </div>
        <div>
          <DropdownMenu headerContent={'Labels'} bodyItems={this.props.availableLabels}
                        inputPlaceholder={'filter labels'} itemClicked={this.props.addLabel}
                        selectedItems={this.props.selectedLabels}/>
        </div>
        <div>
          <DropdownMenu headerContent={'Author'} bodyItems={this.props.availableAuthors}
                        inputPlaceholder={'filter users'} itemClicked={this.props.setAuthor}
                        selectedItems={this.props.selectedAuthor}/>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(connect(mapStateToProps, mapDispatchToProps)(IssueTableHeader));
