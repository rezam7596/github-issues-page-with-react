/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './IssueTableItems.css';
import {connect} from "react-redux";

const mapStateToProps = (state) => {
  return {
    issues: state.issues
  }
};

class IssueTableItems extends React.Component {
  static propTypes = {
    issues: PropTypes.array.isRequired,
  };

  render() {
    return (
      <ul className={s["issue-items"]}>
        {this.props.issues.map(issue => (
          <li key={issue.number.toString()}>
            <div className="fas fa-exclamation"/>
            <div>
              <div>
                {issue.title}
              </div>
              <div>
                #{issue.number} opened {issue["created_at"]} by {issue.user.login}
              </div>
            </div>
          </li>
        ))}
      </ul>
    );
  }
}

export default withStyles(s)(connect(mapStateToProps)(IssueTableItems));
