/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PageHead.css';
import Link from '../Link';
import PropTypes from "prop-types";
import { connect } from 'react-redux'


const mapStateToProps = (state) => {
  return {
    owner: state.owner,
    reposName: state.repository,
    numberOfIssues: state.numberOfIssues,
    numberOfPullRequests: state.numberOfPullRequests,
    numberOfProjects: state.numberOfProjects
  }
};

class PageHead extends React.Component {
  static propTypes = {
    owner: PropTypes.string.isRequired,
    reposName: PropTypes.string.isRequired,
    numberOfIssues: PropTypes.number.isRequired,
    numberOfPullRequests: PropTypes.number.isRequired,
    numberOfProjects: PropTypes.number.isRequired,
  };

  render() {
    return (
      <div className={s.root} role="pagehead">
        <div className={s.container}>
          <div className={s.reposName}>
            {this.props.owner} / <span>{this.props.reposName}</span>
          </div>
          <div className={s.reposInfo}>
            <div>
              <div>Watch</div>
              <div>1,543</div>
            </div>
            <div>
              <div>Star</div>
              <div>51,604</div>
            </div>
            <div>
              <div>Fork</div>
              <div>10,937</div>
            </div>
          </div>
          <div className={s.reposNavigation}>
            <div>Code</div>
            <div className={s.selectedPage}>Issues <span>{this.props.numberOfIssues}</span></div>
            <div>Pull requests <span>{this.props.numberOfPullRequests}</span></div>
            <div>Projects <span>{this.props.numberOfProjects}</span></div>
            <div>Insights</div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(connect(mapStateToProps)(PageHead));
