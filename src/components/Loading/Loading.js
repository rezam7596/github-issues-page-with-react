/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Loading.css';
import ReactLoading from 'react-loading'
import PropTypes from "prop-types";
import {connect} from "react-redux";

const mapStateToProps = (state) => {
  return {
    isLoading: (state.fetching !== 0)
  }
};

class Loading extends React.Component {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
  };

  render() {
    return (
      this.props.isLoading ?
        <div className={s.root}>
          <div className={s.container}>
            <ReactLoading type="spinningBubbles" color="#0000ff" height={30} width={30} />
            <div className={s.text}>
              Loading
            </div>
          </div>
        </div>
        :
        <div/>
    );
  }
}

export default withStyles(s)(connect(mapStateToProps)(Loading));
