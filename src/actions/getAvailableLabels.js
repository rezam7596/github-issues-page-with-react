import constants from '../constants';
import setFetching from "./setFetching";
import fetch from 'node-fetch';

const getAvailableLabels = () => dispatch => {
  dispatch(setFetching(true));
  fetch("https://api.github.com/repos/facebook/create-react-app/labels")
    .then(response => response.text())
    .then(availableLabels => {
      dispatch({
        type: constants.CHANGE_AVAILABLE_LABELS,
        payload: JSON.parse(availableLabels)
      });
      dispatch(setFetching(false));
    }).catch(error => {
    console.log("error on issues fetching (available labels). status:" + error.status);
    dispatch(setFetching(false));
  });
};

export default getAvailableLabels;
