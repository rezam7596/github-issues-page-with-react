import constants from '../constants';
import updateIssues from "./updateIssues";

const setAuthor = value => (dispatch, getState) => {
  if (getState().author === value)
    dispatch({
      type: constants.CHANGE_AUTHOR,
      payload: ''
    });
  else
    dispatch({
      type: constants.CHANGE_AUTHOR,
      payload: value
    });
  dispatch(updateIssues());
};

export default setAuthor;
