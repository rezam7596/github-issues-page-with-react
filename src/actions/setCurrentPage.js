import constants from '../constants';
import updateIssues from "./updateIssues";

const setCurrentPage = value => dispatch => {
  dispatch({
    type: constants.CHANGE_CURRENT_PAGE,
    payload: value
  });
  dispatch(updateIssues());
};

export default setCurrentPage;
