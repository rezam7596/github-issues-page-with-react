import constants from '../constants';
import updateIssues from "./updateIssues";

const setMilestone = value => (dispatch, getState) => {
  if (getState().milestone === value)
    dispatch({
      type: constants.CHANGE_MILESTONE,
      payload: ''
    });
  else
    dispatch({
      type: constants.CHANGE_MILESTONE,
      payload: value
    });
  dispatch(updateIssues());
};

export default setMilestone;
