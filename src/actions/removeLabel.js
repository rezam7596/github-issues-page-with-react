import constants from '../constants';
import updateIssues from "./updateIssues";

const removeLabel = value => (dispatch, getState) => {
  let labels = getState();
  labels = labels.filter(label => label !== value);
  dispatch({
    type: constants.CHANGE_LABELS,
    payload: labels
  });
  dispatch(updateIssues());
};

export default removeLabel;
