import constants from '../constants';

const setFetching = value => (dispatch, getState) => {
  let fetching = (value === true) ? getState().fetching + 1 : (value === false) ? getState().fetching - 1 : 0;
  dispatch ({
    type: constants.CHANGE_FETCHING,
    payload: fetching
  });
};

export default setFetching;
