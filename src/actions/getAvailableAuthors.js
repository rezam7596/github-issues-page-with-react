import constants from '../constants';
import setFetching from "./setFetching";
import fetch from 'node-fetch';

const getAvailableAuthors = () => dispatch => {
  dispatch(setFetching(true));
  fetch("https://api.github.com/users")
    .then(response => response.text())
    .then(availableAuthors => {
      dispatch({
        type: constants.CHANGE_AVAILABLE_AUTHORS,
        payload: JSON.parse(availableAuthors)
      });
      dispatch(setFetching(false));
    }).catch(error => {
    console.log("error on issues fetching (available authors). status:" + error.status);
    dispatch(setFetching(false));
  });
};

export default getAvailableAuthors;
