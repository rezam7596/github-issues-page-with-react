import constants from '../constants';
import updateIssues from "./updateIssues";

const setAssignee = value => (dispatch, getState) => {
  if (getState().assignee === value)
    dispatch({
      type: constants.CHANGE_ASSIGNEE,
      payload: ''
    });
  else
    dispatch({
      type: constants.CHANGE_ASSIGNEE,
      payload: value
    });
  dispatch(updateIssues());
};

export default setAssignee;
