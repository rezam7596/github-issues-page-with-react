import constants from '../constants';

const setIssues = value => {
  return {
    type: constants.CHANGE_ISSUES,
    payload: value
  };
};

export default setIssues;
