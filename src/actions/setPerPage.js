import constants from '../constants';
import updateIssues from "./updateIssues";

const setPerPage = value => dispatch => {
  dispatch({
    type: constants.CHANGE_PER_PAGE,
    payload: value
  });
  dispatch(updateIssues());
};

export default setPerPage;
