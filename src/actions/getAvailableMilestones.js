import constants from '../constants';
import setFetching from "./setFetching";
import fetch from 'node-fetch';

const getAvailableMilestones = () => dispatch => {
  dispatch(setFetching(true));
  fetch("https://api.github.com/repos/facebook/create-react-app/milestones")
    .then(response => response.text())
    .then(availableMilestones => {
      dispatch({
        type: constants.CHANGE_AVAILABLE_MILESTONES,
        payload: JSON.parse(availableMilestones)
      });
      dispatch(setFetching(false));
    }).catch(error => {
    console.log("error on issues fetching (available milestones). status:" + error.status);
    dispatch(setFetching(false));
  });
};

export default getAvailableMilestones;
