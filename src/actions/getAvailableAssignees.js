import constants from '../constants';
import setFetching from "./setFetching";
import fetch from 'node-fetch';

const getAvailableAssignees = () => dispatch => {
  dispatch(setFetching(true));
  fetch("https://api.github.com/repos/facebook/create-react-app/assignees")
    .then(response => response.text())
    .then(availableAssignees => {
      dispatch({
        type: constants.CHANGE_AVAILABLE_ASSIGNEES,
        payload: JSON.parse(availableAssignees)
      });
      dispatch(setFetching(false));
    }).catch(error => {
    console.log("error on issues fetching (available assignees). status:" + error.status);
    dispatch(setFetching(false));
  });
};

export default getAvailableAssignees;
