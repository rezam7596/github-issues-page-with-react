import constants from '../constants';
import updateIssues from "./updateIssues";

const setSortAndDirection = value => (dispatch, getState) => {
  let sort = '';
  let direction = '';
  switch (value.toLowerCase()) {
    case 'newest':
      sort = 'created';
      direction = 'desc';
      break;
    case 'oldest':
      sort = 'created';
      direction = 'asc';
      break;
    case 'most commented':
      sort = 'comments';
      direction = 'desc';
      break;
    case 'least commented':
      sort = 'comments';
      direction = 'asc';
      break;
    case 'recently updated':
      sort = 'updated';
      direction = 'desc';
      break;
    case 'least recently updated':
      sort = 'updated';
      direction = 'asc';
      break;
    default:
      sort = 'created';
      direction = 'desc';
  }
  if (getState().sort === sort && getState().direction === direction) {
    dispatch({
      type: constants.CHANGE_SORT,
      payload: 'created'
    });
    dispatch({
      type: constants.CHANGE_DIRECTION,
      payload: 'desc'
    });
  }
  else {
    dispatch({
      type: constants.CHANGE_SORT,
      payload: sort
    });
    dispatch({
      type: constants.CHANGE_DIRECTION,
      payload: direction
    });
  }
  dispatch(updateIssues());
};

export default setSortAndDirection;
