import setFetching from "./setFetching";
import fetch from 'node-fetch';
import setIssues from "./setIssues";

const updateIssues = value => (dispatch, getState) => {
  dispatch(setFetching(true));
  let labels = getState().labels.reduce((labels, label) => labels + label + ",", "").slice(0, -1);
  fetch("https://api.github.com/repos/facebook/create-react-app/issues?" +
    (getState().author !== "" ? "creator=" + getState().author : "") +
    (labels.length !== 0 ? "&labels=" + labels : "") +
    (getState().milestone !== "" ? "&milestone=" + getState().milestone : "") +
    (getState().assignee !== "" ? "&assignee=" + getState().assignee : "") +
    "&sort=" + getState().sort +
    "&direction=" + getState().direction +
    "&page=" + getState().currentPage +
    "&per_page=" + getState().perPage
  )
    .then(response => response.text())
    .then(issues => {
      dispatch(setIssues(JSON.parse(issues)));
      dispatch(setFetching(false));
    }).catch(error => {
    console.log("error on issues fetching (updating issue). status:" + error.status);
    dispatch(setFetching(false));
  });
}

export default updateIssues;
