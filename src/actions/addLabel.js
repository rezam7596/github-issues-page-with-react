import constants from '../constants';
import updateIssues from "./updateIssues";

const addLabel = value => (dispatch, getState) => {
  let labels = getState().labels;
  if(labels.includes(value))
    labels.splice(labels.indexOf(value), 1);
  else
    labels.push(value);
  dispatch({
    type: constants.CHANGE_LABELS,
    payload: labels
  });
  dispatch(updateIssues());
};

export default addLabel;
