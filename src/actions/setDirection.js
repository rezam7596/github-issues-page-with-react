import constants from '../constants';
import updateIssues from "./updateIssues";

const setDirection = value => dispatch => {
  dispatch({
    type: constants.CHANGE_DIRECTION,
    payload: value
  });
  dispatch(updateIssues());
};

export default setDirection;
