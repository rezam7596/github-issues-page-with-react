import constants from '../constants';
import updateIssues from "./updateIssues";

const setSort = value => dispatch => {
  dispatch({
    type: constants.CHANGE_SORT,
    payload: value
  });
  dispatch(updateIssues());
};

export default setSort;
