import constants from '../constants';
import updateIssues from "./updateIssues";

const setProject = value => (dispatch, getState) => {
  if (getState().project === value)
    dispatch({
      type: constants.CHANGE_PROJECT,
      payload: ''
    });
  else
    dispatch({
      type: constants.CHANGE_PROJECT,
      payload: value
    });
  dispatch(updateIssues());
};

export default setProject;
