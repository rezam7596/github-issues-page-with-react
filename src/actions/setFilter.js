import constants from '../constants';
import updateIssues from "./updateIssues";

const setFilter = value => dispatch => {
  dispatch({
    type: constants.CHANGE_FILTER,
    payload: value
  });
  dispatch(updateIssues());
};

export default setFilter;
