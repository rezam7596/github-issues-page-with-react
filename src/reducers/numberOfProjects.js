import constants from "../constants";

export default function numberOfProjects(state = 0, action) {
  switch (action.type) {
    case constants.CHANGE_NUMBER_OF_PROJECTS:
      return parseInt(action.payload);
    default:
      return state;
  }
}
