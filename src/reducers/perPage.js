import constants from "../constants";

export default function perPage(state = 30, action) {
  switch (action.type) {
    case constants.CHANGE_PER_PAGE:
      return parseInt(action.payload);
    default:
      return state;
  }
}
