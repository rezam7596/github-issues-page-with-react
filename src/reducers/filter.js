import constants from "../constants";

export default function filter(state = [], action) {
  switch (action.type) {
    case constants.CHANGE_FILTER:
      return action.payload;
    default:
      return state;
  }
}
