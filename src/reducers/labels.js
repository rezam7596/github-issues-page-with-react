import constants from "../constants";

export default function labels(state = [], action) {
  switch (action.type) {
    case constants.CHANGE_LABELS:
      return action.payload;
    default:
      return state;
  }
}
