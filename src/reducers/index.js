import { combineReducers } from 'redux';
import user from './user';
import runtime from './runtime';
import owner from "./owner";
import repository from "./repository";
import filter from "./filter";
import numberOfIssues from "./numberOfIssues";
import numberOfPullRequests from "./numberOfPullRequests";
import numberOfProjects from "./numberOfProjects";
import author from "./author";
import labels from "./labels";
import project from "./project";
import availableAuthors from "./availableAuthors";
import availableLabels from "./availableLabels";
import availableProjects from "./availableProjects";
import availableMilestones from "./availableMilestones";
import milestone from "./milestone";
import availableAssignees from "./availableAssignees";
import assignee from "./assignee";
import sort from "./sort";
import direction from "./direction";
import issues from "./issues";
import currentPage from "./currentPage";
import perPage from "./perPage";
import fetching from "./fetching";

export default combineReducers({
  user,
  runtime,
  owner,
  repository,
  filter,
  numberOfIssues,
  numberOfPullRequests,
  numberOfProjects,
  availableAuthors,
  author,
  availableLabels,
  labels,
  availableProjects,
  project,
  availableMilestones,
  milestone,
  availableAssignees,
  assignee,
  sort,
  direction,
  issues,
  currentPage,
  perPage,
  fetching
});
