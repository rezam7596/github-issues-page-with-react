import constants from "../constants";

export default function availableLabels(state = [], action) {
  switch (action.type) {
    case constants.CHANGE_AVAILABLE_LABELS:
      return action.payload;
    default:
      return state;
  }
}
