import constants from "../constants";

export default function availableAuthors(state = [], action) {
  switch (action.type) {
    case constants.CHANGE_AVAILABLE_AUTHORS:
      return action.payload;
    default:
      return state;
  }
}
