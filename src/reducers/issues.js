import constants from "../constants";

export default function issues(state = [], action) {
  switch (action.type) {
    case constants.CHANGE_ISSUES:
      return action.payload;
    default:
      return state;
  }
}
