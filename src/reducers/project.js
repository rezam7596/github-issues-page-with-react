import constants from "../constants";

export default function project(state = "", action) {
  switch (action.type) {
    case constants.CHANGE_PROJECT:
      return action.payload;
    default:
      return state;
  }
}
