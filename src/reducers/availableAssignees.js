import constants from "../constants";

export default function availableAssignees(state = [], action) {
  switch (action.type) {
    case constants.CHANGE_AVAILABLE_ASSIGNEES:
      return action.payload;
    default:
      return state;
  }
}
