import constants from "../constants";

export default function availableProjects(state = [], action) {
  switch (action.type) {
    case constants.CHANGE_AVAILABLE_PROJECTS:
      return action.payload;
    default:
      return state;
  }
}
