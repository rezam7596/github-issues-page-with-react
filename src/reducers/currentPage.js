import constants from "../constants";

export default function currentPage(state = 1, action) {
  switch (action.type) {
    case constants.CHANGE_CURRENT_PAGE:
      return parseInt(action.payload);
    default:
      return state;
  }
}
