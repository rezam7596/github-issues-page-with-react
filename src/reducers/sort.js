import constants from "../constants";

export default function sort(state = "created", action) {
  switch (action.type) {
    case constants.CHANGE_SORT:
      return action.payload;
    default:
      return state;
  }
}
