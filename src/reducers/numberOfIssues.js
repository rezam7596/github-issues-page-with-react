import constants from "../constants";

export default function numberOfIssues(state = 0, action) {
  switch (action.type) {
    case constants.CHANGE_NUMBER_OF_ISSUES:
      return parseInt(action.payload);
    default:
      return state;
  }
}
