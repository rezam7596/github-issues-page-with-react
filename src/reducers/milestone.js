import constants from "../constants";

export default function milestone(state = "*", action) {
  switch (action.type) {
    case constants.CHANGE_MILESTONE:
      return action.payload;
    default:
      return state;
  }
}
