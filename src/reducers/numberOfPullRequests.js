import constants from "../constants";

export default function numberOfPullRequests(state = 0, action) {
  switch (action.type) {
    case constants.CHANGE_NUMBER_OF_PULL_REQUESTS:
      return parseInt(action.payload);
    default:
      return state;
  }
}
