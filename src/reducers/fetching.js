import constants from "../constants";

export default function fetching(state = 0, action) {
  switch (action.type) {
    case constants.CHANGE_FETCHING:
      return action.payload;
    default:
      return state;
  }
}
