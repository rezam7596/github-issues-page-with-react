import constants from "../constants";

export default function assignee(state = "*", action) {
  switch (action.type) {
    case constants.CHANGE_ASSIGNEE:
      return action.payload;
    default:
      return state;
  }
}
