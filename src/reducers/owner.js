import constants from "../constants";

export default function owner(state = "owner", action) {
  switch (action.type) {
    case constants.CHANGE_OWNER:
      return action.payload;
    default:
      return state;
  }
}
