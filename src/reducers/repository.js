import constants from "../constants";

export default function repository(state = "repository", action) {
  switch (action.type) {
    case constants.CHANGE_REPOSITORY:
      return action.payload;
    default:
      return state;
  }
}
