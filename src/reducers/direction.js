import constants from "../constants";

export default function direction(state = "desc", action) {
  switch (action.type) {
    case constants.CHANGE_DIRECTION:
      return action.payload;
    default:
      return state;
  }
}
