import constants from "../constants";

export default function author(state = "", action) {
  switch (action.type) {
    case constants.CHANGE_AUTHOR:
      return action.payload;
    default:
      return state;
  }
}
