import constants from "../constants";

export default function availableMilestones(state = [], action) {
  switch (action.type) {
    case constants.CHANGE_AVAILABLE_MILESTONES:
      return action.payload;
    default:
      return state;
  }
}
